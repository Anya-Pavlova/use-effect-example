import { useState } from "react";

export default function Reply(props) {
  console.log("intent");
  const [repliesNumber, setReplies] = useState(props.bot.replies);
  
  if (repliesNumber !== props.bot.replies) {
    //debugger;
    console.log("intent: setReplies");
    setReplies(props.bot.replies);
  }

  return (
    <>
      <div>BotId: {props.bot.id}</div>
      <div>Replies number props: {props.bot.replies}</div>
      <div>Replies number: {repliesNumber}</div>
    </>
  );
}