import { useState, useEffect } from "react";

export default function Reply(props) {
  console.log("intent effect");
  const [repliesNumber, setReplies] = useState(props.bot.replies);

  useEffect(() => {
    //debugger
    console.log("intent effect: setReplies");
    setReplies(props.bot.replies);
  }, [props.bot.replies]);

  return (
    <>
      <div>BotId: {props.bot.id}</div>
      <div>Replies number props: {props.bot.replies}</div>
      <div>Replies number state: {repliesNumber}</div>
    </>
  );
}