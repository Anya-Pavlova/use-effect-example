import { useState } from "react";
import Intent from "./Intent";
import IntentEffects from "./IntentEffects";

export default function App() {
  const bots = [
    {
      id: 1,
      replies: 12
    },
    {
      id: 2,
      replies: 31
    }
  ];

  const [currentBotId, setCurrentBotId] = useState(0);

  const changeBot = () => {
    const newBotId = currentBotId === 0 ? 1 : 0;
    setCurrentBotId(newBotId);
  };
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <button onClick={changeBot}>Change Bot</button>
      <Intent bot={bots[currentBotId]} />
      {/*<IntentEffects bot={bots[currentBotId]} />*/}
    </div>
  );
}
